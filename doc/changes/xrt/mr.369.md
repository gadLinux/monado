xrt: Add xrt_result_t return type to many compositor functions that previously had no way to indicate failure.
