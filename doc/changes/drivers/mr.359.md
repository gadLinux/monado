psvr: Normalize the rotation to not trip up the client app when it gives the
rotation back to `st/oxr` again.
