build: Allow enabling inter-procedural optimization in CMake GUIs, if supported by platform and compiler.
