OpenXR: Correct the return error code for action and action set localized name
validation.
