OpenXR: Make the function `xrGetReferenceSpaceBoundsRect` at least conform to
the spec without actually implementing it, currently we do not track bounds in
Monado.
