OpenXR: Correctly ensure that the application has called the required get
graphics requirements function when creating a session.
