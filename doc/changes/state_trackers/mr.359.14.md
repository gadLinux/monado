OpenXR: Track the name and localized name for both actions and action sets, that
way we can make sure that there are no duplicates. This is required by the spec.
