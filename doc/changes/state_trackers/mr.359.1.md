OpenXR: Verify that the XrViewConfigurationType is supported by the system as
required by the OpenXR spec in xrEnumerateEnvironmentBlendModes.
